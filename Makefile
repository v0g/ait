# Copyright (c) Volker Diels-Grabsch <v@njh.eu>

.PHONY: default
default: check

.PHONY: check
check:
	shellcheck -f gcc ait
	tidy -quiet -e index.html 2>&1 | sed 's,^line \([0-9]\+\) column \([0-9]\+\) - ,index.html:\1:\2:,'

public: index.html
	rm -rf public
	mkdir public
	cp $^ public/
